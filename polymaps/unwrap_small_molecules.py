import os
import io
import pandas as pd
import numpy as np

data_fname = "npt.12ns.data"
read_str = None

with io.open(data_fname, "r", newline="\n") as rf:
    read_str = rf.read()
box_str = read_str.strip().split("Masses\n\n")[0].strip().split("\n\n")[-1].strip()
box_df = pd.read_csv(io.StringIO(box_str), sep=r"\s+", header=None, index_col=None, names=["l", "h", "", "_"])
[xl, xh], [yl, yh], [zl, zh] = box_df[["l", "h"]].values

atom_str = read_str.strip().split("Atoms # full\n\n")[1].strip().split("\n\n")[0].strip()
atom_df = pd.read_csv(io.StringIO(atom_str), 
                      sep=r"\s+", 
                      header=None, 
                      index_col=None, 
                      keep_default_na=False, 
                      names=["id", "mol", "type", "q", "x", "y", "z", "ix", "iy", "iz", "#", "opls"]).sort_values(by="id").reset_index(drop=True)
atom_df["xs"] = ((atom_df["x"] - xl) / (xh - xl)) % 1.
atom_df["ys"] = ((atom_df["y"] - yl) / (yh - yl)) % 1.
atom_df["zs"] = ((atom_df["z"] - zl) / (zh - zl)) % 1.
atom_df.index = atom_df.index + 1

bond_str = read_str.strip().split("Bonds\n\n")[1].strip().split("\n\n")[0].strip()
bond_df = pd.read_csv(io.StringIO(bond_str), sep=r"\s+", header=None, index_col=None, names=["id", "type", "ai", "aj"]).sort_values(by="id").reset_index(drop=True)
bond_df.index = bond_df.index + 1


# atom_df["com_xs"] = np.nan
# atom_df["com_ys"] = np.nan
# atom_df["com_zs"] = np.nan
atom_df["diff_xs"] = np.nan
atom_df["diff_ys"] = np.nan
atom_df["diff_zs"] = np.nan
grouped_df_list = atom_df.groupby("mol")
v_list = []
for k, v in grouped_df_list:
    v.loc[:, ["diff_xs", "diff_ys", "diff_zs"]] = v[["xs", "ys", "zs"]].values - v[["xs", "ys", "zs"]].values[0, :]
    v_list.append(v)
atom_df = pd.concat(v_list, axis=0)
xs = atom_df["xs"].to_numpy()
ys = atom_df["ys"].to_numpy()
zs = atom_df["zs"].to_numpy()
diff_xs = atom_df["diff_xs"].to_numpy()
diff_ys = atom_df["diff_ys"].to_numpy()
diff_zs = atom_df["diff_zs"].to_numpy()
xs[(diff_xs>0.5)&(xs>0.5)] -= 1
xs[(diff_xs<-0.5)&(xs<0.5)] += 1
ys[(diff_ys>0.5)&(ys>0.5)] -= 1
ys[(diff_ys<-0.5)&(ys<0.5)] += 1
zs[(diff_zs>0.5)&(zs>0.5)] -= 1
zs[(diff_zs<-0.5)&(zs<0.5)] += 1

atom_df.loc[:, ["x", "y", "z"]] = np.array([xs, ys, zs]).T @ np.array([[xh - xl, 0, 0], [0, yh - yl, 0], [0, 0, zh - zl]]) + np.array([xl, yl, zl])
atom_str = atom_df[["id", "mol", "type", "q", "x", "y", "z", "#", "opls"]].to_string(header=None, index=None)
with io.open("unwrapped.data", "w", newline="\n") as wf:
    wf.write(atom_str)