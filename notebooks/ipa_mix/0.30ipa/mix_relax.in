clear
shell          mkdir density
shell          cd density
log            log.langevin_npt

variable       Nprint equal 1000
variable       Text equal 300.0
variable       Pext equal 1.0

units          real
atom_style     full
boundary       p p p

pair_style     lj/cut/tip4p/long 14 13 12 20 0.1500 12.0
kspace_style   pppm/tip4p 1.0e-4
include        ../opls.ff
read_data      ../mix_min.data

thermo         ${Nprint}
thermo_style   custom step dt cpu temp press density pe ke xlo xhi ylo yhi zlo zhi
thermo_modify  flush yes

velocity       all create 300.0 1234 rot no dist gaussian

reset_timestep 0

fix            fxshake all shake 0.001 20 0 b 12 a 20
dump           dump1 all custom ${Nprint} traj.lammpstrj id mol type element mass q x y z ix iy iz
dump_modify    dump1 element C C C O H H H H H H H H H O

fix            fxlangevin all langevin ${Text} ${Text} $(150.0*dt) 1234
fix            fxnph all nph iso ${Pext} ${Pext} $(350*dt)

timestep       1.0
run            20000

write_data     data.* nocoeff
write_data     data.relaxed nocoeff
undump         dump1
unfix          fxlangevin
unfix          fxnph
unfix          fxshake

reset_timestep 0

replicate      4 4 4

fix            fxshake all shake 0.001 20 0 b 12 a 20
fix            fxlangevin all langevin ${Text} ${Text} $(150.0*dt) 1234
fix            fxnph all nph iso ${Pext} ${Pext} $(350*dt)
run            100000

reset_timestep 0

dump           dump2 all custom ${Nprint} traj.langevin.lammpstrj id mol type element mass q x y z ix iy iz
dump_modify    dump2 element C C C O H H H H H H H H H O

run            1000000
write_data     data.langevin

unfix          fxlangevin
unfix          fxnph
undump         dump2

dump           dump3 all custom ${Nprint} traj.npt.lammpstrj id mol type element mass q x y z ix iy iz
dump_modify    dump3 element C C C O H H H H H H H H H O
fix            fxnpt all npt temp ${Text} ${Text} $(150*dt) iso ${Pext} ${Pext} $(350*dt)
run            1000000
undump         dump3

write_data     data.npt
